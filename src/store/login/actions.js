import * as types from '../types'

export default {
  login ({commit}, creds) {
    commit(types.LOGIN) // show spinner
    return new Promise(resolve => {
      setTimeout(() => {
        localStorage.setItem('token', 'JWT')
        commit(types.LOGIN_SUCCESS)
        resolve()
      }, 1000)
    })
  },

  logout ({commit}) {
    localStorage.removeItem('token')
    commit(types.LOGOUT)
  }

}
