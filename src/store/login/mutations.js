import * as types from '../types'

export default {
  [types.LOGIN] (state) {
    state.pending = true
  },

  [types.LOGIN_SUCCESS] (state) {
    state.isLoggedIn = true
    state.pending = false
  },

  [types.LOGOUT] (state) {
    state.isLoggedIn = false
  }
}
