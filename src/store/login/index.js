import actions from '../login/actions'
import mutations from '../login/mutations'
import getters from '../login/getters'

// initial state
const state = {
  isLoggedIn: !!localStorage.getItem('token')
}

export default {
  state,
  mutations,
  actions,
  getters
}
