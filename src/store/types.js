export const RECEIVE_INCIDENTS = 'RECEIVE_INCIDENTS'

export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGOUT = 'LOGOUT'
