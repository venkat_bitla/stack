import api from '../../api/resources'
import * as types from '../types'

export default {
  getIncidents ({ commit }) {
    api.getIncidents().then(response => {
      commit(types.RECEIVE_INCIDENTS, { incidents: response.data })
    }).catch(error => {
      console.log(error)
    })
  }
}
