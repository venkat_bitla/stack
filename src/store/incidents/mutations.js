import * as types from '../types'

export default {
  [types.RECEIVE_INCIDENTS] (state, { incidents }) {
    state.incidents = incidents
  }
}
