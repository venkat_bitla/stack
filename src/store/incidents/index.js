import actions from '../incidents/actions'
import mutations from '../incidents/mutations'
import getters from '../incidents/getters'

// initial state
const state = {
  incidents: []
}

export default {
  state,
  mutations,
  actions,
  getters
}
