import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Dashboard from '@/components/Dashboard'
import Customize from '@/components/Customize'
import Incidents from '@/components/incidents/List'
import Comps from '@/components/comps/List'
import NewIncident from '@/components/incidents/Form'
import NewComponent from '@/components/comps/Form'
import LoginView from '@/components/Login'
import store from '../store/index'

Vue.use(Router)

const routes = [{
  path: '/login',
  meta: { requiresAuth: false },
  name: 'Login',
  component: LoginView
},
{
  path: '/',
  meta: { requiresAuth: true },
  name: 'Index',
  component: Index,
  children: [{
    path: '/dashboard',
    meta: { requiresAuth: true },
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/customize',
    meta: { requiresAuth: true },
    name: 'Customize',
    component: Customize
  },
  {
    path: '/incidents',
    meta: { requiresAuth: true },
    name: 'Incidents',
    component: Incidents
  },
  {
    path: '/incidents/new',
    meta: { requiresAuth: true },
    name: 'NewIncident',
    component: NewIncident
  },
  {
    path: '/components',
    meta: { requiresAuth: true },
    name: 'Components',
    component: Comps
  },
  {
    path: '/components/new',
    meta: { requiresAuth: true },
    name: 'NewComponent',
    component: NewComponent
  }]
}]

const router = new Router({
  routes,
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    console.log('isLoggedIn: ' + store.state.login.isLoggedIn)
    if (!store.state.login.isLoggedIn) {
      next({name: 'Login'})
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
