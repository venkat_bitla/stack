import axios from 'axios'

const API_ROOT = '/api/'

export default {
  getIncidents () {
    return axios.get(API_ROOT + 'incidents')
  }
}
